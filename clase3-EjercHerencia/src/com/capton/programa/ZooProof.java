package com.capton.programa;

import com.capton.herencia.Pajaro;
import com.capton.herencia.Perro;
import com.capton.herencia.Pez;

public class ZooProof {

	public static void main(String[] args) {
		
		Pajaro gorrion = new Pajaro();
		gorrion.setNombre("chuli");
		gorrion.setEdad(5);
		gorrion.setPeso(4.4f);
		System.out.println("Datos del pajaro: " + gorrion.getNombre() 
		+ " y pesa " + gorrion.getPeso() + "grs");
		System.out.println("Aumento de peso: " + gorrion.comer( gorrion.getPeso(), 05));
		System.out.println(gorrion.volar());
		System.out.println("Rejuvenecimiento: " + gorrion.rejuvenecer());
		System.out.println("-----------------------------------------");
		
		
		Perro labrador = new Perro("Loki", 12f, 30f);
		System.out.println("Datos del perro: " + labrador.getNombre() + ", de" + 
		labrador.getEdad() + " y pesa" + labrador.getPeso());
		System.out.println("Aumento de peso: " + labrador.comer(labrador.getPeso(), 20));
		System.out.println(labrador.ladrar());
		System.out.println("Rejuvenecimiento: " + labrador.rejuvenecer());
		System.out.println("-----------------------------------------");
		
		Pez dorado = new Pez("Nemo", 3f, 6f);
		System.out.println("Datos del pez: " + dorado.getNombre() + ", de " + 
		dorado.getEdad() + " y pesa " + dorado.getPeso());
		System.out.println("Aumento de peso: " + dorado.comer(dorado.getPeso(), 20));
		System.out.println(dorado.nadar());
		System.out.println("Rejuvenecimiento: " + dorado.rejuvenecer());
		
	
	
	}

}
