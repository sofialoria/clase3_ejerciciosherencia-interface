package com.captton.inter;

import java.util.ArrayList;

public class MainProof {

	public static void main(String[] args) {
		//usa el metodo anotar implementado por lapicera
		
		Ianotador i = new Lapicera();
		i.anotar("Hola soy lapicera");
		
		Lapicera lapi1 = new Lapicera();
		lapi1.anotar("ahora desde instancia lapicera");
		
		Ianotador c = new Computadora();
		c.anotar("Soy compu");
//c.encender no esta porque el objeto tiene acceso a la intrefaz, no al padre electonico q es el que tiene el metodo encender

		Computadora compu = new Computadora();
		compu.anotar("Desde instancia Computadora");
	
		compu.encender();
		
		
		ArrayList<Ianotador> anotadores = new ArrayList<Ianotador>();
		anotadores.add(c);//compu
		anotadores.add(lapi1);//lapicera
		
		System.out.println("------------------------------");
		
		
		for(Ianotador note: anotadores) {
			
			note.anotar("el mensaje");
			
			if( note instanceof Lapicera) {
				System.out.println("Es lapicera");
				
			}else {
				System.out.println("Es computadora");
				((Computadora)note).encender();
			}
			
		}
	
	
	
	
	
	}

}
