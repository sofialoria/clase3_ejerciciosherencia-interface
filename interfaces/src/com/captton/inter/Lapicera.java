package com.captton.inter;

public class Lapicera implements Ianotador {

	public Lapicera() {
		//clase q extiende al anotador
	}
	
	@Override
	public void anotar(String mensaje) {
		System.out.println("Anoto en un papel: " + mensaje);
	}

}
